# Description
This repository generates reports.


## Variables
Variables which require to be setup in CI/CD -> variable is 'SQUAD_TOKEN' and
'SQUAD_URL'.

Variables that can be changed when creating a new
[pipeline](https://gitlab.com/Linaro/lkft/triggers/report/-/pipelines/new).
Which is:
```
QA_GROUP
QA_PROJECT
QA_BASE_BUILD
QA_BUILD
MAX_BUILDS_THRESHOLD
```

Default variables is set to:
```
QA_GROUP=lkft
QA_PROJECT=linux-stable-rc-linux-4.19.y
QA_BASE_BUILD=
QA_BUILD=latest
MAX_BUILDS_THRESHOLD=10
```

QA_PROJECT which can be any of the 'lkft' projects.
```
- "linux-next-master"
- "linux-mainline-master"
- "linux-stable-rc-linux-6.10.y"
- "linux-stable-rc-linux-6.6.y"
- "linux-stable-rc-linux-6.1.y"
- "linux-stable-rc-linux-5.15.y"
- "linux-stable-rc-linux-5.10.y"
- "linux-stable-rc-linux-5.4.y"
- "linux-stable-rc-linux-4.19.y"
```
